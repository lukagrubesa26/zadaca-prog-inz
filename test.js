const arabicToRoman = require('./roman')

describe('roman numerals test suite', () => {

    it('Should return I for 1', () => {
        expect(arabicToRoman(1)).toBe("I")
    })

    it('Should return II for 2', () => {
        expect(arabicToRoman(2)).toBe("II")
    })

    it('Should return CCLVI for 256', () => {
        expect(arabicToRoman(256)).toBe("CCLVI")
    })

    it('Should return DXII for 512', () => {
        expect(arabicToRoman(512)).toBe("DXII")
    })

    it('Should return MXXIV for 1024', () => {
        expect(arabicToRoman(1024)).toBe("MXXIV")
    })

    it('Should return MMXLVIII for 256', () => {
        expect(arabicToRoman(2048)).toBe("MMXLVIII")
    })


})